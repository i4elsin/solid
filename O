// MARK: - O - The Open Closed Principle

// Принцип открытости/закрытости состоит на том, чтоб мы могли положиться на работу интерфейса и не переживать, что с новой модификацией его работа как то изменится. Данный пример ошибочен тем, что каждый раз, добавляя новые возможности - мы изменяем метод tellBasedOnSpeachLevel(_:).

class BabySpeach_Wrong {}
class ChildSpeach_Wrong {}
class TeenagerSpeach_Wrong {}
class AdultSpeach_Wrong {}

class SpeachApparatus_Wrong {
    
    func tellBasedOnSpeachLevel(_ speachLevel: Any) {
        
        if let _ = speachLevel as? BabySpeach_Wrong {

            print("My first word")

        }

        if let _ = speachLevel as? ChildSpeach_Wrong {

            print("My first sentence")

        }

        if let _ = speachLevel as? TeenagerSpeach_Wrong {

            print("Now I can combine my minds")

        }

        if let _ = speachLevel as? AdultSpeach_Wrong {

            print("Now I can keep up the conversation")

        }
        
    }
    
}

SpeachApparatus_Wrong().tellBasedOnSpeachLevel(BabySpeach_Wrong())





// В данном случае, интерфейс всегда остается неизменным, а логика контролируется через протокол и enumeration. Для масштабирования, нам достаточно добавить новый класс, и вызвать его в enumeration.

protocol SpeachLevel {
    
    func tell()
    
}

enum Maturity {

    case baby, child, teenager, adult

    func reproduceThoughts() {

        switch self {

        case .baby: BabySpeach_Right().tell()
        case .child: ChildSpeach_Right().tell()
        case .teenager: TeenagerSpeach_Right().tell()
        case .adult: AdultSpeach_Right().tell()

        }

    }

}

class SpeachApparatus_Right {
    
    func tellBasedOnMaturity(_ maturity: Maturity) {
        
        maturity.reproduceThoughts()
        
    }
    
}

class BabySpeach_Right: SpeachLevel {
    
    func tell() {
        
        print("My first word")
        
    }
    
}

class ChildSpeach_Right: SpeachLevel {
    
    func tell() {
        
        print("My first sentence")
        
    }
    
}

class TeenagerSpeach_Right: SpeachLevel {
    
    func tell() {
        
        print("Now I can combine my minds")
        
    }
    
}

class AdultSpeach_Right: SpeachLevel {
    
    func tell() {
        
        print("Now I can keep up the conversation")
        
    }
    
}

SpeachApparatus_Right().tellBasedOnMaturity(.baby)