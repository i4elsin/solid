// MARK: - L — The Liskov Substitution Principle
// Принцип подстановки Барбары Лисков состоит на том, что субкласс суперкласса, должен наследовать суперкласс без изменения его поведения. В данном примере ошибка в том, что наш суперкласс Hand может хлопать и писать, RightHand, также, в состоянии это сделать, а вот левой рукой писать не может. Если override метод ничего не делает, то мы нарушаем третий принцип SOLID.

class Hand {
    
    func clap() {
        
        print("Claping")
        
    }
    
    func write() {
        
        print("Writing")
        
    }
    
}

class RightHand_Wrong: Hand {}

class LeftHand_Wrong: Hand {
    
    override func write() {
        
        // Мы не можем писать правой рукой
        
    }
    
}





// Решением может быть создание двух протоколов, один из которых является общим и может быть приписан к обеим рукам, а один из - только к "рабочей".

protocol ThingsToDoBothHands {
    
    func clap()
    
}

protocol ThingsToDoWorkingHand {
    
    func write()
    
}

class RightHand_Right: ThingsToDoBothHands, ThingsToDoWorkingHand {
    
    func write() {
        
        print("Writing by right hand")
        
    }
    
    func clap() {
        
        print("Clapping my left hand")
        
    }
    
}

class LeftHand_Right: ThingsToDoBothHands {
    
    func clap() {
        
        print("Clapping my left hand")
        
    }
    
}