// MARK: - D — The Dependency Inversion Principle
// Принцип инверсии зависимостей базируется на том, что класс верхнего уровня, не должен зависеть от классов нижних уровней. Они оба зависят от абстракции. Данный пример ошибочен тем, что наш класс зависит от типа Restaurant_Wrong. А что если человек не хочет есть в ресторане? Нужно создавать еще один обьект другого класса, к примеру, Home, делать оба обьекта опциональными и на этапе инициализации делать проверку, что не удобно.

class Man_Wrong {
    
    let eatInRestaurant: Restaurant_Wrong
    
    init(eatInRestaurant: Restaurant_Wrong) {
        
        self.eatInRestaurant = eatInRestaurant
        
    }
    
    func takeFood() {
        
        eatInRestaurant.eat()
        
    }
    
}

class Restaurant_Wrong {
    
    func eat() {
        
        print("Today, I'm eat in a restaurant")
        
    }
    
}





// Решением может быть создание протокола, который будет присвоен всем, кто может предоставить нам еду.

protocol Eating {
    
    func eat()
    
}

class Man_Right {
    
    let eatType: Eating
    
    init(eatType: Eating) {
        
        self.eatType = eatType
        
    }
    
    func takeFood() {
        
        eatType.eat()
        
    }
    
}

class Restaurant_Right: Eating {
    
    func eat() {
        
        print("Today, I'm eat in a restaurant")
        
    }
    
}